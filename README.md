# Cyber Security Base - Project I

First project for [Cyber Security Base course](https://cybersecuritybase.github.io/).

> In the first course project, your task is to create a web application that has
> at least five flaws from the OWASP top ten list
> (https://www.owasp.org/index.php/Top_10_2013-Top_10). Starter code for the
> project is provided on Github at
> https://github.com/cybersecuritybase/cybersecuritybase-project.

> You will then write a brief (1000 words) report that outlines how the flaws
> can be first identified and then fixed. For the identification process, we
> suggest that you use tools that have been used in the course, such as Owasp
> ZAP. Once these two tasks have been completed, you will review two projects
> from other course participants.

## Vulnerabilities

- [*A1 - Injection:*](https://www.owasp.org/index.php/Top_10_2013-A1-Injection)
  Todo notes are in a database and the application uses user input directly to
  complete one of his/her notes. For example, `curl -X PUT localhost:8000/todos
  -d 'id=1+or+1+%3D+1'` will complete all notes in the system, and not just the
  one note that was originally planned to. SQLite seems to execute only first
  query, so it doesn't seem to be possible to break out of the `UPDATE` and do
  something else.

## Planned vulnerabilities

- ~~~[*A1 - Injection:*](https://www.owasp.org/index.php/Top_10_2013-A1-Injection) Todo notes are in a database and the application uses user input
  directly to query his/her notes.~~~
- [*A2 - Broken Authentication and Session Management:*](https://www.owasp.org/index.php/Top_10_2013-A2-Broken_Authentication_and_Session_Management)
  Very weak session IDs for authenticated users, e.g. running number assigned to each logged in user.
- [*A3 - Cross-Site Scripting (XSS)*](https://www.owasp.org/index.php/Top_10_2013-A3-Cross-Site_Scripting_(XSS))
  A page shows all recent registered users and e.g. their addresses. Addresses
  won't be escaped, so XSS will be possible.
- [*A6 - Sensitive Data Exposure:*](https://www.owasp.org/index.php/Top_10_2013-A6-Sensitive_Data_Exposure)
  Sensitive user data (e.g. a password) is shown in plaintext or with weak
  crypto (e.g. MD5 without hashing). Then by guessing a session ID of an user,
  the attacker can retrieve this data.
- [*A7 - Missing Function Level Access Control:*](https://www.owasp.org/index.php/Top_10_2013-A7-Missing_Function_Level_Access_Control)
  Certain functionality is missing access control, e.g. deleting a todo item.
- [*A8 - Cross-Site Request Forgery (CSRF):*](https://www.owasp.org/index.php/Top_10_2013-A8-Cross-Site_Request_Forgery_(CSRF))
  Forms won't have any unique tokens, so CSRF is possible.
- [*A10 - Unvalidated Redirects and Forwards:*](https://www.owasp.org/index.php/Top_10_2013-A10-Unvalidated_Redirects_and_Forwards)
  Calculate some redirection based on user parameters.

## Installing

This project uses [Rocket](https://rocket.rs/) as a web framework. To use it,
you'll need nightly version of rust. To get it, you can use `rustup`:

``` bash
$ curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly -y
$ source ~/.cargo/env # Needs to be every time for new terminal
```

## Running

Just execute `cargo run`, and after a while you should see the following:

```
🔧  Configured for development.
    => listening: localhost:8000
    => logging: Normal
🛰  Mounting '/':
    => GET /
🚀  Rocket has launched from http://localhost:8000...
```

Now it is accessible in http://localhost:8000.

------

If you get an error like the following, you are not using nightly but stable rust:

```
error[E0554]: #[feature] may not be used on the stable release channel
 --> ~/.cargo/registry/src/github.com-1ecc6299db9ec823/rocket_codegen-0.1.4/build.rs:1:1
  |
1 | #![feature(slice_patterns)]
  | ^^^^^^^^^^^^^^^^^^^^^^^^^^^
```

Load nightly by executing `source ~/.cargo/env`.

## Development

- Install Diesel command line tool to work with the DB migrations:
  ```
  cargo install diesel_cli --no-default-features --features "sqlite chrono"
  ```

- Setup DB with latest migration:
  ```
  diesel migration --database-url db.sqlite run
  ```

- Redo last DB migration:
  ```
  diesel migration --database-url db.sqlite redo
  ```
