CREATE TABLE todo_items (
  id INTEGER PRIMARY KEY NOT NULL,
  item TEXT NOT NULL,
  finished BOOLEAN NOT NULL DEFAULT 0,
  deadline TEXT NOT NULL
);

INSERT INTO todo_items(item, deadline) VALUES('Take out the thrash', '2017-1-30 9:10:11');
INSERT INTO todo_items(item, deadline) VALUES('Finish cybersecurity project', '2017-1-29 23:59:59');
INSERT INTO todo_items(item, deadline) VALUES('Read OWASP wiki', '2017-2-25 00:00:00');

CREATE TABLE users (
  id INTEGER PRIMARY KEY NOT NULL,
  name TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL,
  address TEXT NOT NULL
);

-- password
INSERT INTO users(name, password, address) VALUES('gary', '5f4dcc3b5aa765d61d8327deb882cf99', 'Fakestreet 123');
-- password123
INSERT INTO users(name, password, address) VALUES('jack', '482c811da5d5b4bc6d497ffa98491e38', 'Sunset Boulevard 1230');
