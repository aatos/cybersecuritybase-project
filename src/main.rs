#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

mod routes;
mod models;
mod db;
mod schema;
mod utils;

extern crate rocket;
extern crate rocket_contrib;

extern crate serde_json;
// TODO: what is `macro_use` for?
#[macro_use]
extern crate serde_derive;

extern crate chrono;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;
extern crate dotenv;

extern crate crypto;

fn main() {
    rocket::ignite()
        .mount("/",
               routes![routes::index,
                       routes::register,
                       routes::login,
                       routes::todos,
                       routes::create_todo,
                       routes::delete_todo,
                       routes::complete_todo])
        .launch();
}
