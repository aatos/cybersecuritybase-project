use db;
use models;

use rocket::http::Status;
use rocket::request::{FlashMessage, Form};
use rocket::response::{Flash, Redirect};
use rocket_contrib::Template;

use std::collections::HashMap;
use utils;

#[get("/")]
fn index(flash: Option<FlashMessage>) -> Template {
    let mut context = HashMap::<&str, String>::new();

    if let Some(flash) = flash {
        context.insert("error", flash.msg().clone().to_string());
    }

    Template::render("index", &context)
}

#[post("/register", data = "<user_form>")]
fn register(user_form: Form<models::NewUser>) -> Result<Template, Flash<Redirect>> {
    let new_user = user_form.get();

    if new_user.name.is_empty() || new_user.password.is_empty() || new_user.address.is_empty() {
        return Err(Flash::error(Redirect::to("/"), "Invalid request."));
    }

    let new_user = models::NewUser { password: utils::md5_string(new_user.password.as_str()), ..new_user.clone() };

    if !try!(db::new_user(&new_user).map_err(|_| Flash::error(Redirect::to("/"), "Internal error."))) {
        return Err(Flash::error(Redirect::to("/"), "User exists."));
    }

    let mut context = HashMap::<&str, &str>::new();
    context.insert("user_name", new_user.name.as_str());

    Ok(Template::render("register_success", &context))
}

#[post("/login", data = "<login_form>")]
fn login(login_form: Form<models::Login>) -> Result<Redirect, Flash<Redirect>> {
    let login = login_form.get();

    if login.name.is_empty() || login.password.is_empty() {
        return Err(Flash::error(Redirect::to("/"), "Invalid request."));
    }

    let user_data = match try!(db::get_user(login.name.as_str())
        .map_err(|_| Flash::error(Redirect::to("/"), "Internal error."))) {
        Some(user) => user,
        None => return Err(Flash::error(Redirect::to("/"), "User not found.")),
    };

    if utils::md5_string(login.password.as_str()) != user_data.password {
        return Err(Flash::error(Redirect::to("/"), "Invalid password."));
    }

    Ok(Redirect::to("/todos"))
}

#[get("/todos")]
fn todos() -> Result<Template, Status> {
    let items = try!(db::get_todos().map_err(|_| Status::InternalServerError));

    let mut context = HashMap::new();
    context.insert("items", items);

    Ok(Template::render("todos", &context))
}

#[post("/todos", data = "<todo_form>")]
fn create_todo(todo_form: Form<models::NewTodoItem>) -> Result<Redirect, Status> {
    let new_todo = todo_form.get();

    if new_todo.item.is_empty() || new_todo.deadline.is_empty() {
        return Err(Status::BadRequest);
    }

    try!(db::new_todo(new_todo).map_err(|_| Status::InternalServerError));

    Ok(Redirect::to("/todos"))
}

#[delete("/todos/<id>")]
fn delete_todo(id: i32) -> Result<Redirect, Status> {
    match db::delete_todo(id) {
        Ok(count) => {
            match count {
                1 => Ok(Redirect::to("/todos")),
                0 => Err(Status::NotFound),
                _ => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::InternalServerError),
    }
}

#[put("/todos/", data = "<complete_form>")]
fn complete_todo(complete_form: Form<models::CompleteTodo>) -> Result<Redirect, Status> {
    let ref id = complete_form.get().id;

    // Passing user provided string to database leads to injection attacks. Even
    // setting id's type to i32 would block this..
    match db::complete_todo(id) {
        Ok(count) => {
            match count {
                1 => Ok(Redirect::to("/todos")),
                0 => Err(Status::NotFound),
                _ => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::InternalServerError),
    }
}
