use crypto::digest::Digest;
use crypto::md5::Md5;

pub fn md5_string(input: &str) -> String {
    let mut md5 = Md5::new();

    md5.input_str(input);

    md5.result_str()
}
