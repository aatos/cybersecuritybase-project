use diesel;
use diesel::LoadDsl;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

use dotenv::dotenv;
use models;
use std::env;

fn establish_connection() -> Result<SqliteConnection, &'static str> {
    dotenv().ok();
    let database_url = match env::var("DATABASE_URL") {
        Ok(url) => url,
        Err(_) => return Err("DATABASE_URL must be set"),
    };

    match SqliteConnection::establish(&database_url) {
        Ok(conn) => Ok(conn),
        Err(_) => Err("Error connecting to database"),
    }
}

pub fn get_todos() -> Result<Vec<models::TodoItem>, &'static str> {
    use schema::todo_items::dsl::*;

    let connection = try!(establish_connection());

    todo_items.load::<models::TodoItem>(&connection).map_err(|_| "Error loading items")
}

pub fn new_todo(new_todo: &models::NewTodoItem) -> Result<(), &'static str> {
    use schema::todo_items;

    let connection = try!(establish_connection());

    diesel::insert(new_todo)
        .into(todo_items::table)
        .execute(&connection)
        .map_err(|_| "Error saving new todo item")
        .map(|_| ())
}

pub fn delete_todo(id: i32) -> Result<usize, &'static str> {
    use schema::todo_items::dsl::todo_items;

    let connection = try!(establish_connection());

    diesel::delete(todo_items.find(id))
        .execute(&connection)
        .map_err(|_| "Error deleting todo item")
}

pub fn complete_todo(id: &str) -> Result<usize, &'static str> {
    use diesel::expression::sql_literal::sql;

    let connection = try!(establish_connection());

    // This is quite insecure
    let sql_statement = format!("UPDATE todo_items SET finished=1 WHERE id={};", id);

    sql::<diesel::types::BigInt>(sql_statement.as_str())
        .execute(&connection)
        .map_err(|_| "Error modifying todo item")
}

pub fn new_user(new_user: &models::NewUser) -> Result<bool, &'static str> {
    use schema::users;

    let connection = try!(establish_connection());

    match diesel::insert(new_user)
        .into(users::table)
        .execute(&connection) {
        Ok(_) => Ok(true),
        Err(e) => {
            match e {
                diesel::result::Error::DatabaseError(e, _) => {
                    match e {
                        diesel::result::DatabaseErrorKind::UniqueViolation => Ok(false),
                        _ => Err("Error saving new user"),
                    }
                }
                _ => Err("Error saving new user"),
            }
        }
    }
}

pub fn get_user(user_name: &str) -> Result<Option<models::User>, &'static str> {
    use schema::users::dsl::*;

    let connection = try!(establish_connection());

    match users.filter(name.eq(user_name))
        .first::<models::User>(&connection) {
        Ok(user) => Ok(Some(user)),
        Err(e) => {
            match e {
                diesel::result::Error::NotFound => Ok(None),
                _ => Err("Error retrieving user"),
            }
        }
    }
}
