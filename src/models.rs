#[derive(Queryable, Serialize, Debug)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub password: String,
    pub address: String,
}

impl ToString for User {
    fn to_string(&self) -> String {
        format!("{}", self.name)
    }
}

use super::schema::users;

#[derive(Insertable, FromForm, Clone, Debug)]
#[table_name="users"]
pub struct NewUser {
    pub name: String,
    pub password: String,
    pub address: String,
}

#[derive(FromForm, Debug)]
pub struct Login {
    pub name: String,
    pub password: String,
}

#[derive(Queryable, Serialize, Debug)]
pub struct TodoItem {
    pub id: i32,
    pub item: String,
    pub finished: bool,
    pub deadline: String, // TODO: use chrono
}

impl ToString for TodoItem {
    fn to_string(&self) -> String {
        format!("{}", self.item)
    }
}

use super::schema::todo_items;

#[derive(Insertable, FromForm, Debug)]
#[table_name="todo_items"]
pub struct NewTodoItem {
    pub item: String,
    pub deadline: String,
}

#[derive(FromForm, Debug)]
pub struct CompleteTodo {
    pub id: String,
}
